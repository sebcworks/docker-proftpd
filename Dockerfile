# Create a proFTPd instance working in passive mode
# An environment file is needed!
# Content:
#
#FTPSERVERNAME Only for cosmetic purpose
#FTPPASVPORT_START Start port of the range of listening port for passive ftp mode
#FTPPASVPORT_END End port of the range
#FTPMASKERADEADDRESS Host IP address
#FTPPASSWD Password of the user to create (ftp)
#FTPUSER Username to create
#FTPUID User to map UID with (for example: www-data)
#FTPGID Group to map GID with
#FTPHOME The home folder for that user
#
# Run by:
# docker run --env-file=env.file -p XXXX:21 -p XXXX-XXXX:FTPPASVPORT_START-FTPPASVPORT_END sebcworks/proftpd -d
# Example:
# docker run --env-file=env.file -p 41249:21 -p 41250-41300:41250-41300 --volumes-from website sebcworks/proftpd -d

FROM debian:jessie

MAINTAINER Sebastien Collin <sebastien.collin@sebcworks.fr>

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
    	    less \
	    nano \
	    proftpd

COPY proftpd-entrypoint.sh /
COPY proftpd-template-conf /tmp/proftpd-template-conf

ENTRYPOINT [ "/proftpd-entrypoint.sh" ]

CMD [ "start" ]
