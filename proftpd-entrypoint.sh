#!/bin/bash

set -e

# Check env variables
check_env() {
    # Check the presence of the environment variables needed by the script. Return 1 if missing
    # an environment var
    RETVAL_ENV=0
    # None needed for now!
    test -z "$FTPPASSWD" && RETVAL_ENV=1
    test -z "$FTPUSER" && RETVAL_ENV=1
    test -z "$FTPUID" && RETVAL_ENV=1
    test -z "$FTPGID" && RETVAL_ENV=1
    test -z "$FTPHOME" && RETVAL_ENV=1
    test -z "$FTPSERVERNAME" && RETVAL_ENV=1
    test -z "$FTPPASVPORT_START" && RETVAL_ENV=1
    test -z "$FTPPASVPORT_END" && RETVAL_ENV=1
    test -z "$FTPMASKERADEADDRESS" && RETVAL_ENV=1
    echo $RETVAL_ENV
}

test -z "`grep '/bin/false' /etc/shells`" && echo "/bin/false" >> /etc/shells

if [ ! -e '/tmp/proftpd.installed' -o "x$1" == "xinstall" ]; then

    if [ $(check_env) -ne 0 ]; then
	echo "FATAL ERROR: Missing necessary environment variables."
	exit 1
    fi

    test -e "/etc/proftpd/ftpd.passwd" && rm "/etc/proftpd/ftpd.passwd"
    test -e "/etc/proftpd/proftpd.conf" && rm "/etc/proftpd/proftpd.conf"
    # Create user account
    echo "${FTPPASSWD}" | ftpasswd --passwd --stdin --name=${FTPUSER} --sha512 --uid=`id -u ${FTPUID}` --gid=`id -g ${FTPGID}` --file=/etc/proftpd/ftpd.passwd --home="${FTPHOME}" --shell=/bin/false
    
    # Make proftpd configuration file
    cp /tmp/proftpd-template-conf /etc/proftpd/proftpd.conf
    sed -i -e "s/\#FTPSERVERNAME\#/$FTPSERVERNAME/" /etc/proftpd/proftpd.conf
    sed -i -e 's/\#FTPPASVPORT_START\#/'$FTPPASVPORT_START'/' /etc/proftpd/proftpd.conf
    sed -i -e 's/\#FTPPASVPORT_END\#/'$FTPPASVPORT_END'/' /etc/proftpd/proftpd.conf
    sed -i -e 's/\#FTPMASKERADEADDRESS\#/'$FTPMASKERADEADDRESS'/' /etc/proftpd/proftpd.conf
    #cat /etc/proftpd/proftpd.conf
fi

# Launch proftpd
echo "Launching proFTPd"
touch "/tmp/proftpd.installed"
exec proftpd --nodaemon
